from django.contrib import admin
from django.contrib.auth.hashers import (is_password_usable, make_password)
from django.contrib.auth.admin import UserAdmin as BaseAdmin
from django.contrib.auth.models import User, Group
from .models import Profile

from import_export.admin import ImportExportModelAdmin, ImportMixin, ImportForm, ConfirmImportForm
from import_export import resources

from django import forms

# Register your models here.
admin.site.register(Profile)

class CustomImportForm(ImportForm):
    group_text = forms.ModelChoiceField(
        queryset=Group.objects.all(),
        required=True)

class CustomConfirmImportForm(ConfirmImportForm):
    group_text = forms.ModelChoiceField(
        queryset=Group.objects.all(),
        required=True)

class UserResource(resources.ModelResource):

    class Meta:
        model = User
    
    def before_import_row(self, row, **kwargs):
        # Group Check
        group = self.request.POST.get('group_text', None)
        
        # Mise en place du contexte pour pouvoir conserver le groupe
        if group is not None :
            self.request.session['import_context_group'] = group
        else:
            try:
                group = self.request.session['import_context_group']
            except KeyError as e:
                raise Exception("Group context failure on row import, " +
                                    "check resources.py for more info: {e}")

        if row['groups'] is None :
            row['groups'] = group
        else:
            row['groups'] += ','+group


        # Password Check
        password_Text = row.get('password')

        if not password_Text.startswith('pbkdf2_sha256') :
            row['password'] = make_password(password_Text)

    def __init__(self, request=None):
        super()
        self.request = request
        print('init')


class CustomUserAdmin(ImportMixin, admin.ModelAdmin):
    resource_class = UserResource

    def get_import_form(self):
        return CustomImportForm

    def get_confirm_import_form(self):
        return CustomConfirmImportForm

    def get_resource_kwargs(self, request, *args, **kwargs):
        rk = super().get_resource_kwargs(request, *args, **kwargs)
        rk['request'] = request
        return rk

    def get_form_kwargs(self, form, *args, **kwargs):
        # pass on `author` to the kwargs for the custom confirm form
        if isinstance(form, CustomImportForm):
            if form.is_valid():
                author = form.cleaned_data['group_text']
                kwargs.update({'test': author.id})
        elif isinstance(form, ConfirmImportForm):
            if form.is_valid():
                author = form.cleaned_data['group_text']
                kwargs.update({'test': author.id})
        return kwargs


admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)