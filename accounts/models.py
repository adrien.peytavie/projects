from django.db import models
from django.contrib.auth.models import User

import os

def content_file_name_Profile(instance, filename):
    ext = str.lower(filename.split('.')[-1])
    filename = "%s.%s" % (instance.user.username, ext)
    dir = "profile_pics"
    return os.path.join('uploads', dir, filename)

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='../static/images/jek_vorobey.jpg', upload_to=content_file_name_Profile)
    
    def __str__(self):
        return "%s Profile " % (self.user.username)
	
