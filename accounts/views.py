# accounts/views.py
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.views import generic
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseRedirect
from django.core.exceptions import ObjectDoesNotExist

from .models import User, Profile

class SignUp(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'

@login_required
def profile(request):
    return render(request, 'accounts/profile.html')

@login_required
def loadProfilePicture(request):
    _user = request.user
    
    if _user.is_active:
        try:
            _profile = Profile.objects.get(user=_user)
        except ObjectDoesNotExist:
            _profile = Profile.objects.create(user=_user)
        
        # Get all parameters
        file = request.FILES['document']
        
        # Save Profile
        _profile.image = file
        _profile.save()
    
    return HttpResponseRedirect(reverse_lazy('account:profile'))