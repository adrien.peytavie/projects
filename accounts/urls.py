# accounts/urls.py
from django.urls import path
from django.contrib.auth.decorators import login_required

from . import views
from accounts import views as user_views

app_name = 'account'
urlpatterns = [
    path('signup/', views.SignUp.as_view(), name='signup'),
	path('profile/', user_views.profile, name='profile'),
	path('load_profile_picture/', user_views.loadProfilePicture, name='load_profile_picture'),
]