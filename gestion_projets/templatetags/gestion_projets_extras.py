from django import template
from datetime import date

register = template.Library()

#@register.filter(name='get_group_order') 
#def getSelGroupOrder(user, session):
    # Select subject of the session
#    listSubject = []
 #   for subject in session.subjects_set.all():
 #       listSubject.append(subject.id)
    # Select order of the project from user
 #   listOrder = project.users.filter(id=user.id)
    # Search index
 #   liste.index("b") 
 #   return "1,2,3,4"

@register.filter(name='has_group') 
def has_group(user, group_name):
    return user.groups.filter(name=group_name).exists() 

@register.filter(name='is_subject_open') 
def is_subject_open(subject, session):
    test = True
    for project in session.project_set.all() :
        if subject.id == project.subject.id :
            if project.is_valid_tutor or project.is_valid_admin :
                return False
    return test 

@register.filter(name='has_project') 
def has_no_project(user, session):
    test = True
    for project in session.project_set.all() :
        if project.users.filter(id=user.id) :
            return False
    return True 

@register.filter(name='has_selectiongroup') 
def has_no_selectiongroup(user, session):
    test = True
    for group in session.projectSelection_set.all() :
        if group.users.filter(id=user.id) :
            return False
    return True 

@register.filter(name='count_user') 
def count_user(subject, session):
    for project in session.project_set.all() :
        if subject.id == project.subject.id :
            return project.users.all().count()
    return 0 

@register.filter(name='exist_report') 
def exist_report(project, date_report):
    return project.report_set.filter(dateReport=date_report).exists()

@register.filter(name='exist_deposit') 
def exist_deposit(project, date_deposit):
    return project.deposit_set.filter(dateDeposit=date_deposit).exists()

@register.filter(name='get_report') 
def get_report(project, date_report):
    return project.report_set.filter(dateReport=date_report)

@register.filter(name='get_deposit') 
def get_deposit(project, date_deposit):
    return project.deposit_set.filter(dateDeposit=date_deposit)


@register.filter(name='count_report') 
def count_report(project):
    return project.report_set.count()

@register.filter(name='count_star_report') 
def count_star_report(project):
    numberStar = 0
    for report in project.report_set.all() :
        if report.checkbox_contact : 
            numberStar = numberStar+1
        if report.checkbox_task_previous : 
            numberStar = numberStar+1
        if report.checkbox_task_next : 
            numberStar = numberStar+1
        if report.checkbox_members : 
            numberStar = numberStar+1
        if report.checkbox_time : 
            numberStar = numberStar+1
    return numberStar

@register.filter(name='count_date_report') 
def count_date_report(session):
    numberReport = 0
    today = date.today()

    for date_report in session.datereport_set.all() :
        if date_report.date_limit <= today : 
            numberReport = numberReport+1
    return numberReport

@register.filter(name='count_star_date_report') 
def count_star_date_report(session):
    numberStar = 0
    today = date.today()

    for date_report in session.datereport_set.all() :
        if date_report.date_limit <= today : 
            numberStar = numberStar+1
    return numberStar * 5

@register.filter(name='count_star_total_report') 
def count_star_total_report(session):
    return session.datereport_set.count() * 5

@register.filter(name='times') 
def times(number):
    return range(number)

@register.filter(name='timesMinus5') 
def timesMinus5(number):
    return range(5-number)

# Stats

@register.filter(name='count_subjects_user') 
def count_subjects_user(user):
    return user.subject_set.count()


@register.filter(name='count_projects_session_user') 
def count_projects_session_user(user, session):
    number = 0

    for project in session.project_set.all() :
        if project.users.count() > 0 :
            if project.subject.user.id == user.id :
                number = number + 1

    return number


@register.filter(name='count_projects_validation_user') 
def count_projects_validation_user(user, session):
    number = 0

    for project in session.project_set.all() :
        if project.subject.user.id == user.id :
            for report in project.report_set.all():
                if not report.is_valid :
                    number = number + 1

    return number


@register.filter(name='count_missing_validation') 
def count_missing_validation(user):
    number = 0

    for subject in user.subject_set.all() :
        for project in subject.project_set.all():
            for report in project.report_set.all():
                if not report.is_valid :
                    number = number + 1

    return number


@register.filter(name='choice_position') 
def choice_position(order, selection):

    if order is None :
        return -3

    if selection=="":
        return -1

    if len(selection.split(','))>1:
        return -2

    _list = order.split(',')

    return _list.index(selection) + 1
    

#@register.simple_tag
#def mul(n1, n2, digit):
#    return round(n1*n2,digit)

@register.filter(name='mul') 
def mul(n1, n2):
    return round(n1*n2,4)


@register.filter(name='round_fun') 
def round_fun(n1, digit):
    return round(n1,int(digit))

@register.filter(name='short_selection') 
def short_selection(text):
    if text is None:
        return ''
    else:
        data = text.split(',')
        data2 = data[:3]
        if len(data)>3 :
            data2.append('...')
        return ','.join(data2)

