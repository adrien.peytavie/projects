from django.contrib import admin
from django.contrib.auth.hashers import (is_password_usable, make_password)

from import_export.admin import ImportExportModelAdmin
from .models import Tag, Technology, Subject, Session, Project, DateDeposit, Deposit, DepositSession, Report, DateReport, ProjectSelection

# Register your models here.
admin.site.register(Tag)
admin.site.register(Technology)
# admin.site.register(Subject)
# admin.site.register(Session)
admin.site.register(Project)
admin.site.register(DepositSession)
admin.site.register(Deposit)
admin.site.register(DateDeposit)
admin.site.register(Report)
admin.site.register(DateReport)
admin.site.register(ProjectSelection)

class DateReportInline(admin.TabularInline):
    model = DateReport
    extra = 5

class SessionAdmin(admin.ModelAdmin):
    inlines = [DateReportInline]
    list_display = ('session_name', 'session_start', 'session_end','phase')
    list_filter = ['session_name','phase']

admin.site.register(Session, SessionAdmin)

@admin.register(Subject)
class SubjectViewAdmin(ImportExportModelAdmin):
    pass
