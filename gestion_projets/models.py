from django.db import models
from django.contrib.auth.models import User, Group

import os
import unidecode

# Tag for a project subject.
class Tag(models.Model):
    text = models.CharField(max_length=50)
    
    def __str__(self):
        return "%s" % (self.text)

# Technology for a project subject.
class Technology(models.Model):
    text = models.CharField(max_length=50)
    
    def __str__(self):
        return "%s" % (self.text)

# Subject for a project.
class Subject(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    
    title = models.CharField(max_length=100)
    description = models.TextField()
    
    technologies = models.ManyToManyField(Technology, blank=True)
    tags = models.ManyToManyField(Tag, blank=True)
    
    difficulty = models.IntegerField(default=0)
    
    motivation = models.TextField(blank=True, null=True)
    is_valid = models.BooleanField(default=False)

    nb_view = models.IntegerField(default=0)
    
    def __str__(self):
        return "%s" % (self.title)

# Session.
class Session(models.Model):
    session_name = models.CharField(max_length=100)
    description = models.TextField(default="")
    
    session_start = models.DateField()
    session_end = models.DateField()
    phase = models.IntegerField(default=0)
    modeGroup = models.IntegerField(default=0)
    enable_ordering = models.BooleanField(default=False)
    
    subjects = models.ManyToManyField(Subject, blank=True)
    group = models.ForeignKey(Group, on_delete=models.CASCADE, blank=True, null=True)

    student_can_submit = models.BooleanField(default=False)
    visibility = models.BooleanField(default=False)
    
    maximum_per_group = models.IntegerField(default=4)

    def __str__(self):
        return "%s, %s" % (self.session_name, self.session_start)
    
    class Meta:
        permissions = [
            ("submit_subject", "Can submit a subject"),
        ]

# Project for a session.
class Project(models.Model):
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    
    users = models.ManyToManyField(User, blank=True)# check tutor or student
    
    is_valid_tutor = models.BooleanField(default=False)
    is_valid_admin = models.BooleanField(default=False)

    git_url = models.CharField(max_length=100, blank=True, null=True)
    
    def __str__(self):
        return "%s, Session[ %s ]" % (self.subject.title, self.session.session_name)


def content_file_name_DepositSession(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (instance.titleDeposit, ext)
    filename = unidecode.unidecode(filename)
    dir = "%s" % (instance.session.session_name)
    dir = dir.replace('/','-')
    dir = unidecode.unidecode(dir)
    return os.path.join('uploads', 'session', dir, filename)

# Deposit Session.
class DepositSession(models.Model):
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    
    titleDeposit = models.CharField(max_length=100)
    upload = models.FileField(upload_to=content_file_name_DepositSession, max_length=500)

    def __str__(self):
        return "%s, Session[ %s ]" % (self.titleDeposit, self.session.session_name)

# Step tasks.
class DateDeposit(models.Model):
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    
    title = models.CharField(max_length=100)
    description = models.TextField()
    date_limit = models.DateField()

    def __str__(self):
        return "%s, Session[ %s ]" % (self.title, self.session.session_name)

    class Meta:
        ordering = ['date_limit']

def content_file_name_Deposit(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (instance.dateDeposit.title, ext)
    filename = unidecode.unidecode(filename)
    dir_session = "%s" % (instance.project.session.session_name)
    dir_session = dir_session.replace('/','-')
    dir_project = "%s" % (instance.project.subject)
    dir_project = dir_project.replace('/','-')
    dir_project = dir_project.replace(' ','_')
    dir_project = unidecode.unidecode(dir_project)
    return os.path.join('uploads', 'session', dir_session, dir_project, filename)

class Deposit(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    dateDeposit = models.ForeignKey(DateDeposit, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    
    description = models.TextField()
    
    upload = models.FileField(upload_to=content_file_name_Deposit, max_length=500)
    date_deposit = models.DateField(blank=True, null=True)

    is_valid = models.BooleanField(default=False)

    note = models.IntegerField(default=0)

    comment = models.TextField(blank=True, null=True)

    def __str__(self):
        return "%s, Project[ %s ], Session[ %s ]" % (self.date_deposit, self.project.subject.title, self.project.session.session_name)

# Step tasks.
class DateReport(models.Model):
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    date_limit = models.DateField(blank=True, null=True)

    def __str__(self):
        return "%s, Session[ %s ]" % (self.date_limit, self.session.session_name)

    class Meta:
        ordering = ['date_limit']

# Step tasks.
class Report(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    dateReport = models.ForeignKey(DateReport, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    
    description = models.TextField()
    
    date_deposit = models.DateField(blank=True, null=True)
    is_valid = models.BooleanField(default=False)

    checkbox_contact = models.BooleanField(default=False)
    checkbox_task_previous = models.BooleanField(default=False)
    checkbox_task_next = models.BooleanField(default=False)
    checkbox_members = models.BooleanField(default=False)
    checkbox_time = models.BooleanField(default=False)

    comment = models.TextField(blank=True, null=True)

    def __str__(self):
        return "%s, Project[ %s ], Session[ %s ]" % (self.date_deposit, self.project.subject.title, self.project.session.session_name)

# Step tasks.
class ProjectSelection(models.Model):

    session = models.ForeignKey(Session, on_delete=models.CASCADE)

    users = models.ManyToManyField(User)
    order = models.TextField(blank=True, null=True)
    order_current = models.TextField(blank=True, null=True)

    date_creation = models.DateTimeField(blank=True, null=True)
    date_modification = models.DateTimeField(blank=True, null=True)

    ranking = models.IntegerField(default=0)
    random_number = models.FloatField(default=0.0)

    assigned = models.BooleanField(default=False)
    project_generated = models.BooleanField(default=False)
    
    def __str__(self):
        return "%s, Session[ %s ]" % (self.order, self.session.session_name)

