from django.urls import path
from django.contrib.auth.decorators import login_required

from .views import *

app_name = 'gestion_projets'
urlpatterns = [
    # Subjects Views
    path('subjects/', login_required(views_subjects.SubjectsView.as_view()), name='subject_list'),
    path('subject/new/', login_required(views_subjects.SubjectNew.as_view()), name='subject_new'),
	path('subject/view/<int:pk>/', login_required(views_subjects.SubjectViewDetailView.as_view()), name='subject_view'),
	path('subject/edit/<int:pk>/', login_required(views_subjects.SubjectEditDetailView.as_view()), name='subject_edit'),
	
    path('subjectFn/<int:subject_id>/', views_subjects.subjectFn, name='subject_Fn'),
    path('subjectFnNew/', views_subjects.subjectFnNew, name='subject_FnNew'),
    
    # Sessions Views
    path('sessions/', login_required(views_sessions.SessionsView.as_view()), name='session_list'),
	path('sessions/view/<int:pk>/', login_required(views_sessions.SessionViewDetailView.as_view()), name='session_view'),

	path('sessions/add_subject/<int:session_id>/<int:subject_id>', views_sessions.SessionAddSubject, name='session_add_subject'),
	path('sessions/remove_subject/<int:session_id>/<int:subject_id>', views_sessions.SessionRemoveSubject, name='session_remove_subject'),
    path('sessions/choose_subject/<int:session_id>/<int:subject_id>', views_sessions.SessionChooseSubject, name='session_choose_subject'),
    
    path('sessions/quit_project/<int:session_id>/<int:project_id>', views_sessions.SessionQuitProject, name='session_quit_project'),
    path('sessions/validate_project/<int:session_id>/<int:project_id>', views_sessions.ProjectValidateTutor, name='project_validate_tutor'),
    path('sessions/validate_project_admin/<int:session_id>/<int:project_id>', views_sessions.ProjectValidateAdmin, name='project_validate_admin'),
    path('sessions/project_remove_student/<int:session_id>/<int:project_id>/<int:user_id>', views_sessions.ProjectRemoveStudent, name='project_remove_student'),
    path('sessions/project_add_student/<int:session_id>/<int:project_id>', views_sessions.ProjectAddStudent, name='project_add_student'),

    path('sessions/create_group_selection/<int:session_id>', views_sessions.CreateGroupSelection, name='create_group_selection'),
    path('sessions/join_group_selection/<int:session_id>/<int:group_id>', views_sessions.JoinGroupSelection, name='join_group_selection'),
    path('sessions/quit_group_selection/<int:session_id>/<int:group_id>', views_sessions.QuitGroupSelection, name='quit_group_selection'),
    path('sessions/save_group_selection/<int:session_id>/<int:group_id>', views_sessions.SaveGroupSelection, name='save_group_selection'),
    path('sessions/create_project_selection/<int:session_id>', views_sessions.CreateProjectSelection, name='create_project_selection'),
    path('sessions/step_project_selection/<int:session_id>', views_sessions.StepProjectSelection, name='step_project_selection'),
    path('sessions/all_step_project_selection/<int:session_id>', views_sessions.AllStepProjectSelection, name='all_step_project_selection'),
    path('sessions/random_ranking_project_selection/<int:session_id>', views_sessions.RandomRankingProjectSelection, name='random_ranking_project_selection'),
    path('sessions/init_order_project_selection/<int:session_id>', views_sessions.InitOrderProjectSelection, name='init_order_project_selection'),

    path('sessions/step_rank_rand_project_selection/<int:session_id>', views_sessions.StepRankRandProjectSelection, name='step_rank_rand_project_selection'),
    path('sessions/all_step_rank_rand_project_selection/<int:session_id>', views_sessions.AllStepRankRandProjectSelection, name='all_step_rank_rand_project_selection'),

    path('sessions/enable_ordering_project_selection/<int:session_id>', views_sessions.EnableOrderingProjectSelection, name='enable_ordering_project_selection'),
    path('sessions/load_ranking_project_selection/<int:session_id>', views_sessions.LoadRankingProjectSelection, name='load_ranking_project_selection'),

    path('sessions/generate_project_project_selection/<int:session_id>', views_sessions.GenerateProjectProjectSelection, name='generate_project_project_selection'),
    path('sessions/delete_project_selection/<int:session_id>/<int:projectsel_id>', views_sessions.DeleteProjectSelection, name='delete_project_selection'),



	path('sessions/new_report/<int:session_id>/<int:project_id>/<int:date_report_id>/', views_sessions.SessionReportNewView, name='session_report_new_view'),
    path('sessions/validate_report_tutor/<int:session_id>/<int:report_id>', views_sessions.ReportValidateTutor, name='report_validate_tutor'),

	path('sessions/new_deposit/<int:session_id>/<int:project_id>/<int:date_deposit_id>/', views_sessions.SessionDepositNewView, name='session_deposit_new_view'),
    path('sessions/validate_deposit_tutor/<int:session_id>/<int:deposit_id>', views_sessions.DepositValidateTutor, name='deposit_validate_tutor'),

	path('sessions/git/<int:session_id>/<int:project_id>/', views_sessions.SessionProjectGitView, name='session_project_git_view'),

    # Sessions Admin Views
    path('sessions/admin/<int:pk>/', login_required(views_sessions_admin.SessionAdminDetailView.as_view()), name='session_admin_view'),
    path('sessions/add_members/<int:session_id>/', views_sessions_admin.SessionAdminAddMembers, name='session_admin_add_members_view'),
    path('sessions/remove_members/<int:session_id>/<int:user_id>', views_sessions_admin.SessionAdminRemoveMembers, name='session_admin_remove_members_view'),
    path('sessions/change_description/<int:session_id>', views_sessions_admin.SessionAdminChangeDescription, name='session_admin_change_description_view'),
    path('sessions/change_phase/<int:session_id>', views_sessions_admin.SessionAdminChangePhase, name='session_admin_change_phase_view'),


    # Project Views
    path('projects/view/<int:pk>/', login_required(views_projects.ProjectViewDetailView.as_view()), name='project_view'),
    path('projects/validate_deposit_tutor/<int:project_id>/<int:deposit_id>', views_projects.DepositValidateTutorProject, name='deposit_validate_project'),
    path('projects/validate_report_tutor/<int:project_id>/<int:report_id>', views_projects.ReportValidateTutorProject, name='report_validate_project'),

    path('projects/testMail/<int:project_id>', views_projects.ReportTestMail, name='report_mail'),

    # Test Views
    path('testOrder/', views_tests.testOrder, name='testOrder')
]
