# Generated by Django 2.2.2 on 2019-07-16 08:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0011_update_proxy_permissions'),
        ('gestion_projets', '0003_auto_20190707_1103'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='session',
            name='user_groups',
        ),
        migrations.AddField(
            model_name='session',
            name='group',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='auth.Group'),
        ),
    ]
