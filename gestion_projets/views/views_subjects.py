from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse
from django.views import generic
from django.contrib.auth.decorators import login_required

from django.utils import timezone

from ..models import Subject, Tag, Technology

from django.core.mail import send_mail


class SubjectsView(generic.ListView):
    template_name = 'subjects/subject_list.html'
    context_object_name = 'latest_subject_list'

    def get_queryset(self):
        """
        Return the last five published questions (not including those set to be
        published in the future).
        """
        return Subject.objects.all()

class SubjectViewDetailView(generic.DetailView):
    model = Subject
    template_name = 'subjects/subject_view_detail.html'

    def get_object(self):
        object = get_object_or_404(Subject, pk=self.kwargs.get("pk"))
        object.nb_view = object.nb_view + 1
        object.save()

        return get_object_or_404(Subject, pk=self.kwargs.get("pk"))


class SubjectEditDetailView(generic.DetailView):
    model = Subject
    template_name = 'subjects/subject_edit_detail.html'

class SubjectNew(generic.ListView):
    model = Subject
    template_name = 'subjects/subject_new.html'

@login_required
def subjectFn(request, subject_id):
    subject = get_object_or_404(Subject, pk=subject_id)
    try:
        data = request.POST.copy()
        title = data.get('title')
        description = data.get('description')
        tags_text = str(data.get('tags'))
        if tags_text is None :
            data_tags = ""
        else :
            data_tags = tags_text.split(",")
        technologies_text = str(data.get('technologies'))
        if technologies_text is None :
            data_tech = ""
        else :
            data_tech = technologies_text.split(",")
        difficulty = int(data.get('difficulty'))
    except (KeyError, Subject.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'subjects/subject_edit_detail.html', {
            'subject': subject,
            'error_message': title,
        })
    else:
        # Add title
        subject.title =title
        # Add Description
        subject.description = description
        # Add Difficulty
        subject.difficulty = difficulty
        # Add Tags
        subject.tags.clear()
        for tempTa in data_tags:
            try:
                objTag = Tag.objects.get(text=tempTa)
            except Tag.DoesNotExist:
                objTag = Tag(text=tempTa)
                objTag.save()
            subject.tags.add(objTag)
        # Add Technologies
        subject.technologies.clear()
        for tempTe in data_tech:
            try:
                objTec = Technology.objects.get(text=tempTe)
            except Technology.DoesNotExist:
                objTec = Technology(text=tempTe)
                objTec.save()
            subject.technologies.add(objTec)
		# Add Difficulty
        subject.save()
        
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('gestion_projets:subject_list'))

@login_required
def subjectFnNew(request):

    data = request.POST.copy()
    tags_text = str(data.get('tags'))
    if tags_text is None :
        data_tags = ""
    else :
        data_tags = tags_text.split(",")
    technologies_text = str(data.get('technologies'))
    if technologies_text is None :
        data_tech = ""
    else :
        data_tech = technologies_text.split(",")
    
    subject = Subject.objects.create(user=request.user)

    # Add title
    subject.title = data.get('title')
    # Add Description
    subject.description = data.get('description')
    # Add Difficulty
    subject.difficulty = int(data.get('difficulty'))
    # Add Tags
    for tempTa in data_tags:
        try:
            objTag = Tag.objects.get(text=tempTa)
        except Tag.DoesNotExist:
            objTag = Tag(text=tempTa)
            objTag.save()
        subject.tags.add(objTag)
    # Add Technologies
    for tempTe in data_tech:
        try:
            objTec = Technology.objects.get(text=tempTe)
        except Technology.DoesNotExist:
            objTec = Technology(text=tempTe)
            objTec.save()
        subject.technologies.add(objTec)
    
    subject.save()
    
    # Always return an HttpResponseRedirect after successfully dealing
    # with POST data. This prevents data from being posted twice if a
    # user hits the Back button.
    return HttpResponseRedirect(reverse('gestion_projets:subject_list'))
