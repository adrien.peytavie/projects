from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse
from django.views import generic
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required

from django.utils import timezone

from ..models import Subject, Session, Project, DateDeposit, Deposit, DateReport, Report, User, ProjectSelection

from datetime import date

from django.core.mail import send_mail

import json


class SessionAdminDetailView(generic.DetailView):
    model = Session
    template_name = 'sessions/session_admin.html'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        user = self.request.user
        # Add in a QuerySet of all the books
        context['subject_list'] = Subject.objects.all()
        context['project_user'] = Project.objects.filter(users__id__contains = user.id,session__id__contains = self.object.id)
        context['group_list'] = ProjectSelection.objects.filter(session__id__contains = self.object.id)
        context['projectSelection_user'] = ProjectSelection.objects.filter(users__id__contains = user.id,session__id__contains = self.object.id)
        context['members'] = User.objects.filter(groups__name=self.object.group)
        return context


@login_required
def SessionAdminAddMembers(request, session_id):
    
    if request.user.groups.filter(name='Admin').exists(): # Vérifie le type d'utilisateur
        session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        _group = session.group

        if request.method == 'POST': # Vérifie la méthode du formulaire
            list_user_string = request.POST.get('description')

            list_user = list_user_string.splitlines()

            for user in list_user:
                # tester existance avec filter ?
                _user = get_object_or_404(User, username=user) # Récupère l'utilisateur

                _group.user_set.add(_user)

        
    return HttpResponseRedirect(reverse('gestion_projets:session_admin_view', args=(session_id,)))

@login_required
def SessionAdminRemoveMembers(request, session_id, user_id):
    
    if request.user.groups.filter(name='Admin').exists(): # Vérifie le type d'utilisateur
        session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        _group = session.group

        _user = get_object_or_404(User, pk=user_id) # Récupère l'utilisateur

        if not (_user.groups.filter(name='Admin').exists()):
            _group.user_set.remove(_user)

    return HttpResponseRedirect(reverse('gestion_projets:session_admin_view', args=(session_id,)))


@login_required
def SessionAdminChangeDescription(request, session_id):
    
    if request.user.groups.filter(name='Admin').exists(): # Vérifie le type d'utilisateur
        session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if request.method == 'POST': # Vérifie la méthode du formulaire
            _description = request.POST.get('description')

            session.description = _description

            session.save()

    return HttpResponseRedirect(reverse('gestion_projets:session_admin_view', args=(session_id,)))


@login_required
def SessionAdminChangePhase(request, session_id):
    
    if request.user.groups.filter(name='Admin').exists(): # Vérifie le type d'utilisateur
        session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if request.method == 'POST': # Vérifie la méthode du formulaire
            _phase = request.POST.get('phase')

            session.phase = _phase

            session.save()

    return HttpResponseRedirect(reverse('gestion_projets:session_admin_view', args=(session_id,)))