from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse
from django.views import generic
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required

from django.utils import timezone

from ..models import Subject, Session, Project, DateDeposit, Deposit, DateReport, Report, User, ProjectSelection

from datetime import date, datetime

from django.core.mail import send_mail

import json


class SessionsView(generic.ListView):
    template_name = 'sessions/session_list.html'
    context_object_name = 'sessions_list'
    
    def get_queryset(self):
        """
        Return the last five published questions (not including those set to be
        published in the future).
        """
        return Session.objects.all().order_by('-session_name')

class SessionViewDetailView(generic.DetailView):
    model = Session
    template_name = 'sessions/session_view.html'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        user = self.request.user
        # Add in a QuerySet of all the books
        context['subject_list'] = Subject.objects.all()
        context['project_user'] = Project.objects.filter(session__id = self.object.id).filter(users__id = user.id)
        context['group_list'] = ProjectSelection.objects.filter(session__id = self.object.id)
        context['projectSelection_user'] = ProjectSelection.objects.filter(users__id = user.id).filter(session__id = self.object.id)
        context['remaining_student'] = User.objects.filter(groups__name='Student').filter(groups__name=self.object.group).exclude(project__in=Project.objects.filter(session__id = self.object.id))
        return context

@login_required
def SessionAddSubject(request, session_id, subject_id):
    
    if request.user.groups.filter(name='Tutor').exists(): # Vérifie le type d'utilisateur
        session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if session.phase == 1: # Vérifie la phase de la session : Ajout de sujets
            subject = get_object_or_404(Subject, pk=subject_id) # Vérifie que le sujet existe
            
            session.subjects.add(subject) # Ajout du sujet à la session
        
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session.id,)))

@login_required
def SessionRemoveSubject(request, session_id, subject_id):

    if request.user.groups.filter(name='Tutor').exists(): # Vérifie le type d'utilisateur
        session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if session.phase == 1: # Vérifie la phase de la session : Ajout de sujets
            subject = get_object_or_404(Subject, pk=subject_id) # Vérifie que le sujet existe
        
            session.subjects.remove(subject) # Suppression du sujet à la session
        
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session_id,)))


@login_required
def SessionChooseSubject(request, session_id, subject_id):

    if request.user.groups.filter(name='Student').exists(): # Vérifie le type d'utilisateur
        _session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if request.user.groups.filter(name=_session.group.name): # Vérifie que l'utilisateur à le droit d'accéder à cette session
            if _session.phase == 2: # Vérifie la phase de la session : Ajout de sujets

                _subject = get_object_or_404(Subject, pk=subject_id) # Vérifie que le sujet existe
        
                # Récupère ou crée le projet
                try:
                    project = Project.objects.get(session=_session, subject=_subject)
                except:
                    project = Project.objects.create(session=_session, subject=_subject)
                
                #TODO : vérifier que le projet n'a pas été validé par l'admin ou le tuteur

                # Ajout si l'étudiant n'a pas de projet dans la session courante
                if Project.objects.filter(session__id = session_id, users__id = request.user.id).count() == 0:
                    project.users.add(request.user)
 
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session_id,)))


@login_required
def SessionQuitProject(request, session_id, project_id):

    if request.user.groups.filter(name='Student').exists(): # Vérifie le type d'utilisateur
        session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if request.user.groups.filter(name=session.group.name):
            if session.phase == 2: # Vérifie la phase de la session : Ajout de sujets
                _project = get_object_or_404(Project, pk=project_id) # Vérifie que le projet existe
        
                #TODO : vérifier que le projet n'a pas été validé par l'admin ou le tuteur
                _project.users.remove(request.user) #Supprime l'utilisateur courant
 
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session_id,)))

@login_required
def ProjectValidateTutor(request, session_id, project_id):

    if request.user.groups.filter(name='Tutor').exists(): # Vérifie le type d'utilisateur
        session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if session.phase == 2: # Vérifie la phase de la session : Ajout de sujets
            _project = get_object_or_404(Project, pk=project_id) # Vérifie que le projet existe

            _project.is_valid_tutor = not _project.is_valid_tutor # Modifie l'état de la validation
            _project.save()
 
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session_id,)))

@login_required
def ProjectValidateAdmin(request, session_id, project_id):

    if request.user.groups.filter(name='Admin').exists():  # Vérifie le type d'utilisateur
        session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if session.phase == 2: # Vérifie la phase de la session : Ajout de sujets
            _project = get_object_or_404(Project, pk=project_id) # Vérifie que le projet existe

            _project.is_valid_admin = not _project.is_valid_admin # Modifie l'état de la validation
            _project.save()
 
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session_id,)))

@login_required
def ProjectRemoveStudent(request, session_id, project_id, user_id):

    if request.user.groups.filter(name='Admin').exists():  # Vérifie le type d'utilisateur
        session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if session.phase == 2: # Vérifie la phase de la session : Ajout de sujets
            _project = get_object_or_404(Project, pk=project_id) # Vérifie que le projet existe

            _user = get_object_or_404(User, pk=user_id) # Récupère l'utilisateur

            _project.users.remove(_user) #Supprime l'utilisateur
 
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session_id,))+ '#table_admin_projects')

@login_required
def ProjectAddStudent(request, session_id, project_id):

    if request.user.groups.filter(name='Admin').exists():  # Vérifie le type d'utilisateur
        session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if session.phase == 2: # Vérifie la phase de la session : Ajout de sujets
            _project = get_object_or_404(Project, pk=project_id) # Vérifie que le projet existe

            if request.method == 'POST': # Vérifie la méthode du formulaire
                user_id = request.POST.get('student')
                if not (user_id is None):
                    _user = get_object_or_404(User, pk=user_id) # Récupère l'utilisateur

                    _project.users.add(_user)
 
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session_id,))+ '#table_admin_projects')

@login_required
def SessionReportNewView(request, session_id, project_id, date_report_id):

    if request.user.groups.filter(name='Student').exists(): # Vérifie le type d'utilisateur
        session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if request.user.groups.filter(name=session.group.name): # Vérifie que l'utilisateur à le droit d'accéder à cette session
            if session.phase == 3: # Vérifie la phase de la session : Ajout de sujets
                
                # Get Parameters data
                proj = get_object_or_404(Project, pk=project_id)
                date_report = get_object_or_404(DateReport, pk=date_report_id)

                if not Report.objects.filter(project__id=proj.id,dateReport__id=date_report.id).exists():
                    # Get POST data
                    data = request.POST.copy()
                    desc = data.get('description')
                    
                    # Create Report Object
                    report = Report.objects.create(project=proj,dateReport=date_report, user=request.user, description = desc)
                    report.date_deposit = date.today()
                    report.save()
                    
                    send_mail(
                        '[Projets] Compte rendu - '+proj.subject.title,
                        'Bonjour, \n\nun compte rendu a été déposé sur le site des projets.\n\nSujet : '+proj.subject.title+'\nLien du projet : http://iutabgdinlpvm-45.iutbourg.univ-lyon1.fr/gestion_projets/projects/view/'+str(project_id)+'/\n\nCordialement,\nLes responsables de projets',
                        'noreply@univ-lyon1.fr',
                        [proj.subject.user.email],
                        fail_silently=False,
                    )

    # Retour à la fiche de session 
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session_id,)))

@login_required
def ReportValidateTutor(request, session_id, report_id):

    if request.user.groups.filter(name='Tutor').exists(): # Vérifie le type d'utilisateur
        session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if request.user.groups.filter(name=session.group): # Vérifie que l'utilisateur à le droit d'accéder à cette session
            if session.phase == 3: # Vérifie la phase de la session : Ajout de sujets
                
                _report = get_object_or_404(Report, pk=report_id) # Vérifie que le report existe

                # Mise à jour du rapport
                data = request.POST.copy()

                _report.checkbox_contact = data.get('checkbox_contact')=='on'
                _report.checkbox_task_previous = data.get('checkbox_task_previous')=='on'
                _report.checkbox_task_next = data.get('checkbox_task_next')=='on'
                _report.checkbox_members = data.get('checkbox_members')=='on'
                _report.checkbox_time = data.get('checkbox_time')=='on'
                _report.comment = data.get('comment')

                print(data.get('comment'))

                _report.is_valid = True
                _report.save()
 
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session_id,)))

@login_required
def SessionDepositNewView(request, session_id, project_id, date_deposit_id):
    
    if request.user.groups.filter(name='Student').exists(): # Vérifie le type d'utilisateur
        session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if request.user.groups.filter(name=session.group.name): # Vérifie que l'utilisateur à le droit d'accéder à cette session
            if session.phase == 3: # Vérifie la phase de la session : Ajout de sujets
               
                # Get Parameters data
                proj = get_object_or_404(Project, pk=project_id)
                date_deposit = get_object_or_404(DateDeposit, pk=date_deposit_id)

                if not Deposit.objects.filter(project__id=proj.id,dateDeposit__id=date_deposit.id).exists():
                    # Get POST data
                    data = request.POST.copy()
                    desc = data.get('description')
                    file = request.FILES['document']

                    # Create Report Object
                    deposit = Deposit.objects.create(project=proj,dateDeposit=date_deposit, user=request.user, description = desc, upload=file)
                    deposit.date_deposit = date.today()
                    deposit.save()
					
                    send_mail(
                        '[Projets] Document - '+proj.subject.title,
                        'Bonjour, \n\nun nouveau document a été déposé sur le site des projets.\n\nSujet : '+proj.subject.title+'\nType de document : '+deposit.dateDeposit.title+
						'\nLien du projet : http://iutabgdinlpvm-45.iutbourg.univ-lyon1.fr/gestion_projets/projects/view/'+str(project_id)+'/\n\nCordialement,\nLes responsables de projets',
                        'noreply@univ-lyon1.fr',
                        [proj.subject.user.email],
                        fail_silently=False,
                    )

    # Retour à la fiche de session 
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session_id,)))

@login_required
def DepositValidateTutor(request, session_id, deposit_id):
    
    if request.user.groups.filter(name='Tutor').exists(): # Vérifie le type d'utilisateur
        session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if request.user.groups.filter(name=session.group): # Vérifie que l'utilisateur à le droit d'accéder à cette session
            if session.phase == 3: # Vérifie la phase de la session : Ajout de sujets
                
                _deposit = get_object_or_404(Deposit, pk=deposit_id) # Vérifie que le depot existe
                
                # Mise à jour du dépôt
                data = request.POST.copy()
                _deposit.note = int(data.get('note'))

                _deposit.is_valid = True
                _deposit.save()
 
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session_id,)))

@login_required
def SessionProjectGitView(request, session_id, project_id):

    if request.user.groups.filter(name='Student').exists(): # Vérifie le type d'utilisateur
        session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if request.user.groups.filter(name=session.group.name): # Vérifie que l'utilisateur à le droit d'accéder à cette session
            if session.phase == 3: # Vérifie la phase de la session : Ajout de sujets
                
                # Get Parameters data
                proj = get_object_or_404(Project, pk=project_id)

                # Get POST data
                data = request.POST.copy()
                git = data.get('git_url')
                
                proj.git_url = git # Modify Project
                proj.save()

    # Retour à la fiche de session 
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session_id,)))


@login_required
def CreateGroupSelection(request, session_id):
    if request.user.groups.filter(name='Student').exists(): # Vérifie le type d'utilisateur
        _session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if request.user.groups.filter(name=_session.group.name): # Vérifie que l'utilisateur à le droit d'accéder à cette session
            if _session.phase == 2: # Vérifie la phase de la session : Ajout de sujets
                #Vérifie que l'utilisateur ne fait pas parti d'un groupe
                if ProjectSelection.objects.filter(session__id = session_id,users__id = request.user.id).count()==0 :
                
                    # Get Parameters data
                    proj_sel = ProjectSelection.objects.create(session=_session)
                    
                    proj_sel.users.add(request.user)
                    proj_sel.save()

    # Retour à la fiche de session 
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session_id,)))

@login_required
def JoinGroupSelection(request, session_id, group_id):
    if request.user.groups.filter(name='Student').exists(): # Vérifie le type d'utilisateur
        session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if request.user.groups.filter(name=session.group.name): # Vérifie que l'utilisateur à le droit d'accéder à cette session
            if session.phase == 2: # Vérifie la phase de la session : Ajout de sujets
                #Vérifie que l'utilisateur ne fait pas parti d'un groupe
                if ProjectSelection.objects.filter(session__id = session_id,users__id = request.user.id).count()==0 :

                    # Get Parameters data
                    proj_sel = get_object_or_404(ProjectSelection, pk=group_id)
                    
                    proj_sel.users.add(request.user)

    # Retour à la fiche de session 
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session_id,)))

@login_required
def QuitGroupSelection(request, session_id, group_id):
    if request.user.groups.filter(name='Student').exists(): # Vérifie le type d'utilisateur
        session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if request.user.groups.filter(name=session.group.name): # Vérifie que l'utilisateur à le droit d'accéder à cette session
            if session.phase == 2: # Vérifie la phase de la session : Ajout de sujets

                # Get Parameters data
                proj_sel = get_object_or_404(ProjectSelection, pk=group_id)
                
                proj_sel.users.remove(request.user)

                if proj_sel.users.count() == 0 :
                    proj_sel.delete()

    # Retour à la fiche de session 
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session_id,)))


@login_required
def SaveGroupSelection(request, session_id, group_id):
    if request.user.groups.filter(name='Student').exists(): # Vérifie le type d'utilisateur
        session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if request.user.groups.filter(name=session.group.name): # Vérifie que l'utilisateur à le droit d'accéder à cette session
            if session.phase == 2: # Vérifie la phase de la session : Ajout de sujets
                
                # Get Parameters data
                proj_sel = get_object_or_404(ProjectSelection, pk=group_id)
                
                data = request.POST.copy()
                proj_sel.order = data.get('order')
                proj_sel.order_current = proj_sel.order

                proj_sel.date_modification = datetime.now()

                proj_sel.save()

    return HttpResponse(
            json.dumps({"nothing to see": "this isn't happening"}),
            content_type="application/json"
        )

import random
random.seed(datetime.now().timestamp())


# Seulement 1 groupe par personne
@login_required
def CreateProjectSelection(request, session_id):
    
    if request.user.groups.filter(name='Admin').exists(): # Vérifie le type d'utilisateur
        _session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if request.user.groups.filter(name=_session.group.name): # Vérifie que l'utilisateur à le droit d'accéder à cette session
            if _session.phase == 2: # Vérifie la phase de la session : Ajout de sujets

                # liste des utilisateurs
                _users = User.objects.filter(groups__name='Student').filter(groups__name=_session.group)#.exclude(groups__name='Admin')

                # ranking aleatoire
                list_ranking = [i for i in range(1, _users.count()+1)]
                random.shuffle(list_ranking)

                # liste de sujets
                list_sel = []
                for subject in _session.subjects.all() :
                    list_sel.append(str(subject.id))
                
                i = 0
                for user in _users :

                    #Vérifie que l'utilisateur ne fait pas parti d'un groupe
                    if ProjectSelection.objects.filter(session__id = session_id, users__id = user.id).count()==0 :
                    
                        # Get Parameters data
                        proj_sel = ProjectSelection.objects.create(session=_session)
                        
                        proj_sel.users.add(user)

                        random.shuffle(list_sel) # mélange de la liste pour ne pas biaiser
                        proj_sel.order = ','.join(list_sel)
                        proj_sel.order_current = proj_sel.order

                        proj_sel.date_creation = datetime.now()

                        proj_sel.ranking = list_ranking[i]
                        proj_sel.random_number = random.random()

                        # Generate list random
                        proj_sel.save()
                        i= i+1
        
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session_id,)))

# Seulement 1 groupe par personne
@login_required
def StepProjectSelection(request, session_id):
    
    if request.user.groups.filter(name='Admin').exists(): # Vérifie le type d'utilisateur
        _session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if request.user.groups.filter(name=_session.group.name): # Vérifie que l'utilisateur à le droit d'accéder à cette session
            if _session.phase == 2: # Vérifie la phase de la session : Ajout de sujets

                projects = ProjectSelection.objects.filter(session__id = session_id).order_by('ranking').exclude(assigned = True)

                # First element
                _object = get_object_or_404(ProjectSelection, pk=projects[0].id) # Vérifie que le sujet existe
                _list_sel = (_object.order_current).split(',')
                _object.order_current = _list_sel[0]
                _object.assigned = True # A pour effet d'exclure l'object du calcul
                _object.save()

                value = _list_sel[0]
                for i in range(0, projects.count()) :
                    # enleve sujet affecté
                    _object = get_object_or_404(ProjectSelection, pk=projects[i].id) # Vérifie que le sujet existe

                    _list_sel = (_object.order_current).split(',')
                    _list_sel.remove(value)
                    _object.order_current = ','.join(_list_sel)

                    _object.save()
        
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session_id,)))


# Seulement 1 groupe par personne
@login_required
def AllStepProjectSelection(request, session_id):
    
    if request.user.groups.filter(name='Admin').exists(): # Vérifie le type d'utilisateur
        _session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if request.user.groups.filter(name=_session.group.name): # Vérifie que l'utilisateur à le droit d'accéder à cette session
            if _session.phase == 2: # Vérifie la phase de la session : Ajout de sujets

                projects = ProjectSelection.objects.filter(session__id = session_id).order_by('ranking').exclude(assigned = True)

                while projects.count()>0 :
                    # First element
                    _object = get_object_or_404(ProjectSelection, pk=projects[0].id) # Vérifie que le sujet existe
                    _list_sel = (_object.order_current).split(',')
                    _object.order_current = _list_sel[0]
                    _object.assigned = True # A pour effet d'exclure l'object du calcul
                    _object.save()

                    value = _list_sel[0]
                    for i in range(0, projects.count()) :
                        # enleve sujet affecté
                        _object = get_object_or_404(ProjectSelection, pk=projects[i].id) # Vérifie que le sujet existe

                        _list_sel = (_object.order_current).split(',')
                        _list_sel.remove(value)
                        _object.order_current = ','.join(_list_sel)

                        _object.save()
        
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session_id,)))


# Seulement 1 groupe par personne
@login_required
def RandomRankingProjectSelection(request, session_id):
    
    if request.user.groups.filter(name='Admin').exists(): # Vérifie le type d'utilisateur
        _session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if request.user.groups.filter(name=_session.group.name): # Vérifie que l'utilisateur à le droit d'accéder à cette session
            if _session.phase == 2: # Vérifie la phase de la session : Ajout de sujets

                projects = ProjectSelection.objects.filter(session__id = session_id)

                # ranking aleatoire
                list_ranking = [i for i in range(1, projects.count()+1)]
                random.shuffle(list_ranking)

                for i in range(0, projects.count()) :
                    # enleve sujet affecté
                    _object = get_object_or_404(ProjectSelection, pk=projects[i].id) # Vérifie que le sujet existe

                    _object.ranking = list_ranking[i]
                    _object.random_number = random.random()

                    _object.save()
        
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session_id,)))


# Seulement 1 groupe par personne
@login_required
def InitOrderProjectSelection(request, session_id):
    
    if request.user.groups.filter(name='Admin').exists(): # Vérifie le type d'utilisateur
        _session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if request.user.groups.filter(name=_session.group.name): # Vérifie que l'utilisateur à le droit d'accéder à cette session
            if _session.phase == 2: # Vérifie la phase de la session : Ajout de sujets

                projects = ProjectSelection.objects.filter(session__id = session_id)

                for i in range(0, projects.count()) :
                    # enleve sujet affecté
                    _object = get_object_or_404(ProjectSelection, pk=projects[i].id) # Vérifie que le sujet existe

                    _object.order_current = _object.order
                    _object.assigned = False

                    _object.save()
        
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session_id,)))


# Seulement 1 groupe par personne
@login_required
def StepRankRandProjectSelection(request, session_id):
    
    if request.user.groups.filter(name='Admin').exists(): # Vérifie le type d'utilisateur
        _session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if request.user.groups.filter(name=_session.group.name): # Vérifie que l'utilisateur à le droit d'accéder à cette session
            if _session.phase == 2: # Vérifie la phase de la session : Ajout de sujets

                projects = ProjectSelection.objects.filter(session__id = session_id).order_by('ranking').exclude(assigned = True)
                unsorted_results = projects.all()
                projects = sorted(unsorted_results, key= lambda A: A.ranking*A.random_number )

                # First element
                _object = get_object_or_404(ProjectSelection, pk=projects[0].id) # Vérifie que le sujet existe
                _list_sel = (_object.order_current).split(',')
                _object.order_current = _list_sel[0]
                _object.assigned = True # A pour effet d'exclure l'object du calcul
                _object.save()

                value = _list_sel[0]
                for i in range(1, len(projects)) :
                    # enleve sujet affecté
                    _object = get_object_or_404(ProjectSelection, pk=projects[i].id) # Vérifie que le sujet existe

                    _list_sel = (_object.order_current).split(',')
                    _list_sel.remove(value)
                    _object.order_current = ','.join(_list_sel)

                    _object.save()
        
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session_id,))+ '#table_admin_projects')


# Seulement 1 groupe par personne
@login_required
def AllStepRankRandProjectSelection(request, session_id):
    
    if request.user.groups.filter(name='Admin').exists(): # Vérifie le type d'utilisateur
        _session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if request.user.groups.filter(name=_session.group.name): # Vérifie que l'utilisateur à le droit d'accéder à cette session
            if _session.phase == 2: # Vérifie la phase de la session : Ajout de sujets

                projects = ProjectSelection.objects.filter(session__id = session_id).order_by('ranking').exclude(assigned = True)
                unsorted_results = projects.all()
                projects = sorted(unsorted_results, key= lambda A: A.ranking*A.random_number )

                while len(projects)>0 :
                    # First element
                    _object = get_object_or_404(ProjectSelection, pk=projects[0].id) # Vérifie que le sujet existe
                    _list_sel = (_object.order_current).split(',')
                    _object.order_current = _list_sel[0]
                    _object.assigned = True # A pour effet d'exclure l'object du calcul
                    _object.save()

                    value = _list_sel[0]
                    for i in range(1, len(projects)) :
                        # enleve sujet affecté
                        _object = get_object_or_404(ProjectSelection, pk=projects[i].id) # Vérifie que le sujet existe

                        _list_sel = (_object.order_current).split(',')
                        _list_sel.remove(value)
                        _object.order_current = ','.join(_list_sel)

                        _object.save()

                    del projects[0]
    
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session_id,))+ '#table_admin_projects')



# Seulement 1 groupe par personne
@login_required
def EnableOrderingProjectSelection(request, session_id):
    
    if request.user.groups.filter(name='Admin').exists(): # Vérifie le type d'utilisateur
        _session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if request.user.groups.filter(name=_session.group.name): # Vérifie que l'utilisateur à le droit d'accéder à cette session
            if _session.phase == 2: # Vérifie la phase de la session : Ajout de sujets

                _session.enable_ordering = not( _session.enable_ordering)
                _session.save()
    
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session_id,))+ '#table_admin_projects')


# Seulement 1 groupe par personne
@login_required
def LoadRankingProjectSelection(request, session_id):
    
    if request.user.groups.filter(name='Admin').exists(): # Vérifie le type d'utilisateur
        _session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if request.user.groups.filter(name=_session.group.name): # Vérifie que l'utilisateur à le droit d'accéder à cette session
            if _session.phase == 2: # Vérifie la phase de la session : Ajout de sujets

                if request.method == 'POST': # Vérifie la méthode du formulaire
                    list_lines_string = request.POST.get('description')

                    list_lines = list_lines_string.splitlines()

                    for line in list_lines:
                        if line != '':
                            data = line.split('\t')
                            # tester existance avec filter ?
                            _project = get_object_or_404(ProjectSelection, id=data[0]) # Récupère l'utilisateur
                            _project.ranking = data[1]

                            _project.save()
    
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session_id,))+ '#table_admin_projects')

# Seulement 1 groupe par personne
@login_required
def GenerateProjectProjectSelection(request, session_id):
    
    if request.user.groups.filter(name='Admin').exists(): # Vérifie le type d'utilisateur
        _session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if request.user.groups.filter(name=_session.group.name): # Vérifie que l'utilisateur à le droit d'accéder à cette session
            if _session.phase == 2: # Vérifie la phase de la session : Ajout de sujets

                projects = ProjectSelection.objects.filter(session__id = session_id)
 
                for i in range(0, len(projects)) :              
                    # Enleve sujet affecté
                    _object = get_object_or_404(ProjectSelection, pk=projects[i].id) # Vérifie que le sujet existe
                    
                    subject_id = _object.order_current
                    subject_id = subject_id.split(',')

                    # Test si 1 seul sujet
                    if len(subject_id)==1 :
                        subject_id = subject_id[0]
                        # Test si sujet est bon
                        if subject_id != "" :
            
                            _subject = get_object_or_404(Subject, pk=int(subject_id)) # Vérifie que le sujet existe
                            
                            # TODO Vérifier si existe
                            _project = Project.objects.create(session=_session, subject=_subject)

                            for user in _object.users.all():
                                _project.users.add(user)

                            _project.save()

                            _object.project_generated = True
                            _object.save()
    
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session_id,))+ '#table_admin_projects')

# Seulement 1 groupe par personne
@login_required
def DeleteProjectSelection(request, session_id, projectsel_id):
    
    if request.user.groups.filter(name='Admin').exists(): # Vérifie le type d'utilisateur
        _session = get_object_or_404(Session, pk=session_id) # Vérifie que la session existe
        
        if request.user.groups.filter(name=_session.group.name): # Vérifie que l'utilisateur à le droit d'accéder à cette session
            if _session.phase == 2: # Vérifie la phase de la session : Ajout de sujets

                _object = get_object_or_404(ProjectSelection, pk=projectsel_id) # Vérifie que le sujet existe

                _object.delete()

    
    return HttpResponseRedirect(reverse('gestion_projets:session_view', args=(session_id,))+ '#table_admin_projects')
