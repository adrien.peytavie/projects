from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse
from django.views import generic
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required

from django.utils import timezone

from ..models import Subject, Session, Project, DateDeposit, Deposit, DateReport, Report, User, ProjectSelection

from datetime import date

from django.core.mail import send_mail

import json



class ProjectViewDetailView(generic.DetailView):
    model = Project
    template_name = 'projects/project_view.html'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        user = self.request.user
        # Add in a QuerySet of all the books
        context['session'] = self.object.session
        return context

@login_required
def DepositValidateTutorProject(request, project_id, deposit_id):
    
    if request.user.groups.filter(name='Tutor').exists(): # Vérifie le type d'utilisateur
        project = get_object_or_404(Project, pk=project_id) # Vérifie que le projet existe
        
        if request.user.groups.filter(name=project.session.group): # Vérifie que l'utilisateur à le droit d'accéder à cette session
            if project.session.phase == 3: # Vérifie la phase de la session : Ajout de sujets
                if request.user == project.subject.user or request.user.groups.filter(name='Admin').exists() :# si tuteur du projet ou Admin

                    _deposit = get_object_or_404(Deposit, pk=deposit_id) # Vérifie que le depot existe
                    
                    # Mise à jour du dépôt
                    data = request.POST.copy()
                    _deposit.note = int(data.get('note'))

                    _deposit.is_valid = True
                    _deposit.save()
 
    return HttpResponseRedirect(reverse('gestion_projets:project_view', args=(project_id,)))

@login_required
def ReportValidateTutorProject(request, project_id, report_id):

    if request.user.groups.filter(name='Tutor').exists(): # Vérifie le type d'utilisateur
        project = get_object_or_404(Project, pk=project_id) # Vérifie que le projet existe
        
        if request.user.groups.filter(name=project.session.group): # Vérifie que l'utilisateur à le droit d'accéder à cette session
            if project.session.phase == 3: # Vérifie la phase de la session : Ajout de sujets
                if request.user == project.subject.user or request.user.groups.filter(name='Admin').exists() :# si tuteur du projet ou Admin
                       
                    _report = get_object_or_404(Report, pk=report_id) # Vérifie que le report existe

                    # Mise à jour du rapport
                    data = request.POST.copy()

                    _report.checkbox_contact = data.get('checkbox_contact')=='on'
                    _report.checkbox_task_previous = data.get('checkbox_task_previous')=='on'
                    _report.checkbox_task_next = data.get('checkbox_task_next')=='on'
                    _report.checkbox_members = data.get('checkbox_members')=='on'
                    _report.checkbox_time = data.get('checkbox_time')=='on'
                    _report.comment = data.get('comment')

                    _report.is_valid = True
                    _report.save()
 
    return HttpResponseRedirect(reverse('gestion_projets:project_view', args=(project_id,)))



@login_required
def ReportTestMail(request, project_id):

    if request.user.groups.filter(name='Tutor').exists(): # Vérifie le type d'utilisateur
        project = get_object_or_404(Project, pk=project_id) # Vérifie que le projet existe
        
        if request.user.groups.filter(name=project.session.group): # Vérifie que l'utilisateur à le droit d'accéder à cette session
            if project.session.phase == 3: # Vérifie la phase de la session : Ajout de sujets
                if request.user == project.subject.user or request.user.groups.filter(name='Admin').exists() :# si tuteur du projet ou Admin
                       
                    send_mail(
                        'Mettre le sujet',
                        'Here is the message.',
                        'adrien.peytavie@univ-lyon1.fr',
                        ['adrien.peytavie@liris.cnrs.fr'],
                        fail_silently=False,
                    )
    return HttpResponseRedirect(reverse('gestion_projets:project_view', args=(project_id,)))
