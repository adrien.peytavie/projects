from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse
from django.views import generic
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required

from django.utils import timezone

from ..models import Subject, Session, Project, DateDeposit, Deposit, DateReport, Report, User, ProjectSelection

from datetime import date

from django.core.mail import send_mail

import json



@login_required
def testOrder(request):# add session id and group
    data = request.POST.copy()
    selection = ProjectSelection.objects.create(order=data.get('order'))
    selection.save()
    # 'text,test'.split(',')

    return HttpResponse(
            json.dumps({"nothing to see": "this isn't happening"}),
            content_type="application/json"
        )
 
