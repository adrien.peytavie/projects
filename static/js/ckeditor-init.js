// static/js/ckeditor-init.js

import {
    ClassicEditor,
    Essentials,
    Paragraph,
    Bold,
    Italic,
    Font,
    Link,
    MediaEmbed,
    Table,
    BlockQuote,
    List,
    Image,
    ImageUpload,
    ImageCaption,
    ImageResize,
    ImageStyle,
    ImageToolbar,
    Base64UploadAdapter,
    SourceEditing,
} from 'ckeditor5';

function initializeCKEditor(selector) {
    ClassicEditor
        .create(document.querySelector(selector), {
            plugins: [
                Essentials, Image, ImageUpload, ImageCaption,
                ImageResize, ImageStyle, Base64UploadAdapter,
                ImageToolbar, Paragraph, Bold, Italic, Font, Link,
                MediaEmbed, Table, BlockQuote, List,
                SourceEditing
            ],
            toolbar: [
                'undo', 'redo', '|', 'heading', '|', 'fontFamily', 'fontSize', 'fontColor', 'fontBackgroundColor',
                '|', 'bold', 'italic', '|', 'link', 'uploadImage', 'insertTable', 'blockQuote', 'mediaEmbed',
                '|', 'bulletedList', 'numberedList', '|', 'sourceEditing'
            ],
            image: {
                resizeOptions: [
                    { name: 'resizeImage:original', label: 'Default image width', value: null },
                    { name: 'resizeImage:50', label: '50% page width', value: '50' },
                    { name: 'resizeImage:75', label: '75% page width', value: '75' },
                ],
                toolbar: [
                    'imageTextAlternative', 'toggleImageCaption', '|', 'imageStyle:inline',
                    'imageStyle:wrapText', 'imageStyle:breakText', '|', 'resizeImage'
                ],
            },
        })
        .then(editor => {
            window.editor = editor;
        })
        .catch(error => {
            console.error(error);
        });
}

export { initializeCKEditor };